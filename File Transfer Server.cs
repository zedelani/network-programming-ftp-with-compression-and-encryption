﻿//Zsalsabilla Pasya Edelani.
//4210181008

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.IO.Compression;
using System.Security;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.Linq;
using System.Threading.Tasks;

//Server
namespace FileTransfer
{
    class Program
    {
        private static string directoryPath = @".\1";
        static void Main(string[] args)
        {
            DirectoryInfo directorySelected = new DirectoryInfo(directoryPath);

            //Receive File.

            // Listen on port 1234    
            TcpListener tcpListener = new TcpListener(IPAddress.Any, 1234);
            tcpListener.Start();

            Console.WriteLine("Server started");

            //Infinite loop to connect to new clients    
            while (true)
            {
                // Accept a TcpClient    
                TcpClient tcpClient = tcpListener.AcceptTcpClient();

                Console.WriteLine("Connected to client");

                StreamReader reader = new StreamReader(tcpClient.GetStream());

                // The first message from the client is the file size    
                string cmdFileSize = reader.ReadLine();

                // The first message from the client is the filename    
                string cmdFileName = reader.ReadLine();

                int length = Convert.ToInt32(cmdFileSize);
                byte[] buffer = new byte[length];
                int received = 0;
                int read = 0;
                int size = 1024;
                int remaining = 0;

                // Read bytes from the client using the length sent from the client    
                while (received < length)
                {
                    remaining = length - received;
                    if (remaining < size)
                    {
                        size = remaining;
                    }

                    read = tcpClient.GetStream().Read(buffer, received, size);
                    received += read;
                }

                // Save the file using the filename sent by the client    
                using (FileStream fStream = new FileStream(Path.GetFileName(cmdFileName), FileMode.Create))
                {
                    fStream.Write(buffer, 0, buffer.Length);
                    fStream.Flush();
                    fStream.Close();
                }

                Console.WriteLine("File received and saved in " + Environment.CurrentDirectory);

                //Decryption File.
                Console.WriteLine("Please type in the filepath of the file you wish to decrypt. ");
                string startFilePath = Console.ReadLine();
                Console.WriteLine("");

                Console.WriteLine("What would you like to call your  decrypted file?");
                Console.WriteLine("Please include the full filepath.");
                string endFilePath = Console.ReadLine();
                Console.WriteLine("");

                Console.WriteLine("Please type in your password. This is your secret key.");
                string secretKey = Console.ReadLine();
                Console.WriteLine("");

                Console.WriteLine("You are about to  decrypt the file located at " + startFilePath);
                Console.WriteLine("Once  decrypted, it will be saved as a separate file called " + endFilePath);
                Console.WriteLine("If this is correct, press enter to continue.");
                Console.ReadLine();

                string decryptedFilePath = Console.ReadLine();

                DecryptFile(startFilePath, endFilePath, secretKey);
                Console.WriteLine("Congrats! Your file is now decrypted.");

                //Decompression File.
                foreach (FileInfo fileToDecompress in directorySelected.GetFiles("*.gz"))
                {
                    Decompress(fileToDecompress);
                }
            }
        }

        //Fungsi Decrypt menggunakan AES.
        public static byte[] AES_Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    decryptedBytes = ms.ToArray();
                }
            }

            return decryptedBytes;
        }

        //Fungsi Descrypt.
        public static void DecryptFile(string fileEncrypted, string file, string password)
        {
            //string fileEncrypted = "C:\\SampleFileEncrypted.DLL";
            //string password = "abcd1234";

            byte[] bytesToBeDecrypted = File.ReadAllBytes(fileEncrypted);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes);

            //string file = "C:\\SampleFile.DLL";

            File.WriteAllBytes(file, bytesDecrypted);
        }


        //Fungsi Decompress file sesuai directorypath-nya.
        public static void Decompress(FileInfo fileToDecompress)
        {
            using (FileStream originalFileStream = fileToDecompress.OpenRead())
            {
                string currentFileName = fileToDecompress.FullName;
                string newFileName = currentFileName.Remove(currentFileName.Length - fileToDecompress.Extension.Length);

                using (FileStream decompressedFileStream = File.Create(newFileName))
                {
                    using (GZipStream decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
                    {
                        decompressionStream.CopyTo(decompressedFileStream);
                        Console.WriteLine($"Decompressed: {fileToDecompress.Name}");
                    }
                }
            }
        }

    }
}