﻿//Zsalsabilla Pasya Edelani
//4210181008

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.IO.Compression;
using System.Security;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.Linq;
using System.Threading.Tasks;

//Client
namespace FileTransferClient
{
    namespace AES_Encrypt_Decrypt //For encryption
    {
        class Program
        {

            private static string directoryPath = @".\1";

            //Fungsi untuk mengompress data sesuai directorypath.
            public static void Compress(DirectoryInfo directorySelected)
            {
                foreach (FileInfo fileToCompress in directorySelected.GetFiles())
                {
                    using (FileStream originalFileStream = fileToCompress.OpenRead())
                    {
                        if ((File.GetAttributes(fileToCompress.FullName) &
                           FileAttributes.Hidden) != FileAttributes.Hidden & fileToCompress.Extension != ".gz")
                        {
                            using (FileStream compressedFileStream = File.Create(fileToCompress.FullName + ".gz"))
                            {
                                using (GZipStream compressionStream = new GZipStream(compressedFileStream,
                                   CompressionMode.Compress))
                                {
                                    originalFileStream.CopyTo(compressionStream);

                                }
                            }
                            FileInfo info = new FileInfo(directoryPath + Path.DirectorySeparatorChar + fileToCompress.Name + ".gz");
                            Console.WriteLine($"Compressed {fileToCompress.Name} from {fileToCompress.Length.ToString()} to {info.Length.ToString()} bytes.");
                        }

                    }
                }
            }

            //Fungsi encrypt menggunakan AES.
            public static byte[] AES_Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
            {
                byte[] encryptedBytes = null;

                // Set your salt here, change it to meet your flavor:
                // The salt bytes must be at least 8 bytes.
                byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

                using (MemoryStream ms = new MemoryStream())
                {
                    using (RijndaelManaged AES = new RijndaelManaged())
                    {
                        AES.KeySize = 256;
                        AES.BlockSize = 128;

                        var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                        AES.Key = key.GetBytes(AES.KeySize / 8);
                        AES.IV = key.GetBytes(AES.BlockSize / 8);

                        AES.Mode = CipherMode.CBC;

                        using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                            cs.Close();
                        }
                        encryptedBytes = ms.ToArray();
                    }
                }

                return encryptedBytes;
            }

            //Fungsi encrypt data.
            public static void EncryptFile(string file, string fileEncrypted, string password)
            {

                byte[] bytesToBeEncrypted = File.ReadAllBytes(file);
                byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

                // Hash the password with SHA256
                passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

                byte[] bytesEncrypted = AES_Encrypt(bytesToBeEncrypted, passwordBytes);

                File.WriteAllBytes(fileEncrypted, bytesEncrypted);
            }

            static void Main(string[] args)
            {
                try
                {
                    //Compression File.
                    DirectoryInfo directorySelected = new DirectoryInfo(directoryPath);
                    Compress(directorySelected);

                    //Encryption File.
                    Console.WriteLine("Please type in the filepath of the file you wish to encrypt. ");
                    string startFilePath = Console.ReadLine();
                    Console.WriteLine("");

                    Console.WriteLine("What would you like to call your  encrypted file?");
                    Console.WriteLine("Please include the full filepath.");
                    string endFilePath = Console.ReadLine();
                    Console.WriteLine("");

                    Console.WriteLine("Please type in your password. This is your secret key.");
                    string secretKey = Console.ReadLine();
                    Console.WriteLine("");

                    Console.WriteLine("You are about to  encrypt the file located at " + startFilePath);
                    Console.WriteLine("Once encrypted, it will be saved as a separate file called " + endFilePath);
                    Console.WriteLine("If this is correct, press enter to continue.");
                    Console.ReadLine();

                    EncryptFile(startFilePath, endFilePath, secretKey);
                    Console.WriteLine("Congrats! Your file is now encrypted.");

                    Console.WriteLine("Please enter a full file path");
                    string fileName = endFilePath;

                    //Sending File.
                    TcpClient tcpClient = new TcpClient("127.0.0.1", 1234);
                    Console.WriteLine("Connected. Sending file.");

                    StreamWriter sWriter = new StreamWriter(tcpClient.GetStream());

                    byte[] bytes = File.ReadAllBytes(fileName);

                    sWriter.WriteLine(bytes.Length.ToString());
                    sWriter.Flush();

                    sWriter.WriteLine(fileName);
                    sWriter.Flush();

                    Console.WriteLine("Sending file");
                    tcpClient.Client.SendFile(fileName);



                }
                catch (Exception e)
                {
                    Console.Write(e.Message);
                }

                Console.Read();
            }
        }
    }
}